import java.awt.*;
import java.awt.event.*;
import java.util.Random;

public class Tank {
	private class BloodBar {
		public void draw(Graphics g) { 
			Color c = g.getColor(); 
			g.setColor(Color.RED);
		g.drawRect(x, y - 10, WIDTH, 10); 
		int w = WIDTH * life / 100; 
		g.fillRect(x, y - 10, w, 10); 
		g.setColor(c);
		}
		}

	BloodBar blood=new BloodBar();
	public static final int XSPEED = 5;
	public static final int YSPEED = 5;

	public static final int WIDTH = 30;
	public static final int HEIGHT = 30;

//保留TankClient的引用，更方便地使用其中的成员变量
	TankClient tc = null;
	private int life=100;

	public int getLife() {
		return this.life;
	}
	public void setLife(int l) {
		this.life=l;
	}
	private int x, y;

	private static Random r = new Random(); 
	Direction[] dirs= Direction.values();
	//将这个枚举类型转为数据组
	 int step=0;
	int rn = r.nextInt(dirs.length);
//是否按下了4个方向键
	int oldX,oldY;
	private boolean live=true;
	
	public boolean isLive() {
		return live;
	}
	public void setLive(boolean l) {
		this.live=l;
	}
	
	public Rectangle getRect() {
		return new Rectangle(x, y, WIDTH, HEIGHT);
		}

	private boolean bL = false, bU = false, bR = false, bD = false;
	private Direction ptDir = Direction.D;

	//成员变量：方向
		enum Direction {
			L, LU, U, RU, R, RD, D, LD, STOP
		};
	 
		private Direction dir = Direction.STOP;
		private boolean good;
		public Tank(int x, int y) {
			this.x = x;
			this.y = y;
		}
		public boolean isGood() {
			return this.good;
		}

		public Tank(int x, int y, boolean good) {
		this.x = x;
		this.y = y;
		this.good = good;
		}
		public Tank(int x, int y, boolean good, TankClient tc) { 
			this.x = x;
			this.y = y;
			this.good = good;
		this.tc = tc;
		}
		public boolean eat(Blood b) {
			if(this.live && b.isLive() &&
			this.getRect().intersects(b.getRect())) { 
				this.life = 100; 
				b.setLive(false);
			return true;
			}
			return false;
			}
		
		public void draw(Graphics g) {
			if(good) {
				blood.draw(g);
				}
			if(!live) return;
			Color c = g.getColor();
			//以不同的颜色来显示不同的坦克
			if(good) {
			g.setColor(Color.RED);
			}
			else {
			g.setColor(Color.BLUE);
			}
			g.fillOval(x, y, WIDTH, HEIGHT);
			g.setColor(c);

			
			if(!good) {
				//Direction.values()将这个枚举类型转为数组Direction[] dirs = Direction.values(); if(step == 0) {
				Direction[] dirs = Direction.values(); if(step == 0) {
					step = r.nextInt(12) + 3;
					int rn = r.nextInt(dirs.length); 
					dir = dirs[rn];
					}
					step--;

					if(r.nextInt(40) > 38) 
						this.fire();
				
			}
			this.oldX = x;
			this.oldY = y;
			move();
			switch(ptDir) {
			case L:
			g.drawLine(x + Tank.WIDTH /2 , y + Tank.HEIGHT / 2, x, y + Tank.HEIGHT / 2);
			break; case LU:
			g.drawLine(x + Tank.WIDTH /2 , y + Tank.HEIGHT /
			2, x, y);
			break;
			case U:
			g.drawLine(x + Tank.WIDTH /2 , y + Tank.HEIGHT / 2, x + Tank.WIDTH /2, y);
			break; case RU:
			g.drawLine(x + Tank.WIDTH /2 , y + Tank.HEIGHT / 2, x + Tank.WIDTH, y);
			break; case R:
			g.drawLine(x + Tank.WIDTH /2 , y + Tank.HEIGHT / 2, x + Tank.WIDTH, y + Tank.HEIGHT / 2);
			break; case RD:
			g.drawLine(x + Tank.WIDTH /2 , y + Tank.HEIGHT / 2, x + Tank.WIDTH, y + Tank.HEIGHT);
			break; case D:
			g.drawLine(x + Tank.WIDTH /2 , y + Tank.HEIGHT / 2, x + Tank.WIDTH /2, y + Tank.HEIGHT);
			break; case LD:
			g.drawLine(x + Tank.WIDTH /2 , y + Tank.HEIGHT / 2, x, y + Tank.HEIGHT);
			break; case STOP:
			break;
			}



		}
		
		void move() {
			switch (dir) {
			case L:
				x -= XSPEED;
				break;
			case LU:
				x -= XSPEED;
				y -= YSPEED;
				break;
			case U:
				y -= YSPEED;
				break;
			case RU:
				x += XSPEED;
	 
				y -= YSPEED;
				break;
			case R:
				x += XSPEED;
				break;
			case RD:
				x += XSPEED;
				y += YSPEED;
				break;
			case D:
				y += YSPEED;
				break;
			case LD:
				x -= XSPEED;
				y += YSPEED;
				break;
			case STOP:
				break;
			}
			if(this.dir!=Direction.STOP) {
				this.ptDir = this.dir;
			}
			
			// 检查是否超出边界
		    if (x < 0) {
		        // 超出左边界，恢复到移动前的位置
		        x = oldX;
		    }
		    if (x > TankClient.GAME_WIDTH - Tank.WIDTH) {
		        // 超出右边界，恢复到移动前的位置
		        x = oldX;
		    }
		    if (y <Tank.WIDTH) {
		        // 超出上边界，恢复到移动前的位置
		        y =oldY ;
		    }
		    if (y > TankClient.GAME_HEIGHT - Tank.HEIGHT) {
		        // 超出下边界，恢复到移动前的位置
		        y = oldY;
		    }
		}
		private void stay() { 
			x = oldX;
		y = oldY;
		}
	    
		
		public boolean collidesWithWall(Wall w) {
			if(this.getRect().intersects(w.getRect()) &&
			this.live) {
			this.stay(); 
			return true;
			}
			return false;
			}
		public void KyePressed(KeyEvent e) {
			int key = e.getKeyCode();
			switch (key) {
	//按下Ctrl时作出的动作
			case KeyEvent.VK_A: 
				superFire(); 
			break;
			case KeyEvent.VK_CONTROL:
				//tc.m = fire();
				break;
			case KeyEvent.VK_LEFT:
				bL = true;
				break;
			case KeyEvent.VK_UP:
				bU = true;
				break;
			case KeyEvent.VK_RIGHT:
				bR = true;
				break;
			case KeyEvent.VK_DOWN:
				bD = true;
				break;
			}
			locateDirection();
		}

		public void kyeReleased(KeyEvent e) {
			int key = e.getKeyCode();
			switch (key) {
			//按下Ctrl时作出动作
			//只有当键被抬起来时，这枚炮弹才被发出去
			//避免了一直按下Ctrl而使得子弹过多的问题
			case KeyEvent.VK_A: 
				superFire(); 
			break;
			case KeyEvent.VK_CONTROL: 
				fire();
			break;

			case KeyEvent.VK_LEFT:
				bL = false;
				break;
			case KeyEvent.VK_UP:
				bU = false;
				break;
			case KeyEvent.VK_RIGHT:
				bR = false;
				break;
			case KeyEvent.VK_DOWN:
				bD = false;
				break;
			}
			locateDirection();
		}

		void locateDirection() {
			if (bL && !bU && !bR && !bD)
				dir = Direction.L;
			else if (bL && bU && !bR && !bD)
				dir = Direction.LU;
			else if (!bL && bU && !bR && !bD)
				dir = Direction.U;
			else if (!bL && bU && bR && !bD)
				dir = Direction.RU;
			else if (!bL && !bU && bR && !bD)
				dir = Direction.R;
			else if (!bL && !bU && bR && bD)
				dir = Direction.RD;
			else if (!bL && !bU && !bR && bD)
				dir = Direction.D;
			else if (bL && !bU && !bR && bD)
				dir = Direction.LD;
			else if (!bL && !bU && !bR && !bD)
				dir = Direction.STOP;
		}
		public boolean collidesWithTanks(java.util.List<Tank> tanks) {
			for(int i = 0; i < tanks.size(); i++) { 
				Tank t = tanks.get(i);
			if(this != t) {
			if(this.live && t.isLive() &&
			this.getRect().intersects(t.getRect())) { 
				t.stay();
			this.stay();
			return true;
			}
			}
			}
			return false;
			}
		public Missile fire(Direction dir) {
			if(!live) return null;
			//保证子弹从Tank的中间出现
			int x = this.x + Tank.WIDTH / 2 - Missile.WIDTH / 2;
			int y = this.y + Tank.HEIGHT /2 - Missile.WIDTH / 2;
			//将Tank现在的位置和方向传递给子弹
			//并且现在子弹的初始化不再是由坦克决定，而是由炮筒决定
			Missile m = new Missile(x, y,  dir, this.tc,this.good); 
			tc.missiles.add(m);
			return m;
			}
		private void superFire() {
			Direction[] dirs = Direction.values();
			for(int i = 0; i < 8; i++) { fire(dirs[i]);
			}
			}
		public Missile fire() {
	//保证子弹从Tank的中间出现
			int x = this.x + Tank.WIDTH / 2 - Missile.WIDTH / 2;
			int y = this.y + Tank.HEIGHT / 2 - Missile.WIDTH / 2;
	//将Tank现在的位置和方向传递给子弹
			
			Missile m = new Missile(x, y, ptDir,this.tc,this.good); 
			tc.missiles.add(m);
			return m;
		}
		public int getX() {
			// TODO Auto-generated method stub
			return this.x;
		}
		public int getY() {
			// TODO Auto-generated method stub
			return this.y;
		}
		
		
	}