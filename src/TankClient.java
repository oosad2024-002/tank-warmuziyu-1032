import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
public class TankClient extends Frame {
	Blood  blood = new Blood();
	public TankClient() {
        // 其他初始化代码		
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                blood.move(); // 更新血块位置
                blood.setLive(true);
            }
        }, 0, 5000); // 每10秒执行一次，调整间隔时间来控制速度
    }

	public static final int GAME_WIDTH = 800;
	public static final int GAME_HEIGHT = 600;
	List<Missile> missiles = new ArrayList<Missile>();
	List<Explode> explodes = new ArrayList<Explode>();
	List<Tank> tanks = new ArrayList<Tank>();
	List<Wall> wall=new ArrayList<Wall>();
 	
 	Tank myTank = new Tank(400, 300,true, this);
	Missile m = null;
	//new 出来了一辆敌方的坦克
	//Tank enemyTank = new Tank(100, 100, false, this);


//这是一张虚拟图片
	Image offScreenImage = null;
	Font font1 = new Font("Arial", Font.PLAIN, 20);
//paint这个方法不需要被调用，一旦要被重画的时会被自动 调用
	public void paint(Graphics g) {
		g.drawString("missiles count: " + missiles.size(), 10, 50);
		g.drawString("explodes count: " + explodes.size(), 10,70);
		g.drawString("tanks count: " + tanks.size(), 10, 90);		 
		
		g.drawString("myTank life: " + this.myTank.getLife(), 10, 110);	
		 g.setFont(font1);
			if(tanks.size()==1&&myTank.isLive()) {
				for(int i = 0; i < 10; i++) {
					Tank t=new Tank(50 + 40 * (i + 1), 50, false, this);						
					tanks.add(t);						
					}

			}
			if(!myTank.isLive()) {
			    Font font = new Font("宋体", Font.PLAIN, 60);
			    g.setFont(font);
				g.drawString("游戏结束！ 按P复活 " , 150, 400);	
			}
		//将容器中的炮弹逐个画出来
		for(int i = 0; i < missiles.size(); i++) { 
			Missile m = missiles.get(i);
			m.hitTanks(tanks);		
			
			for(Wall w:wall) {
				m.hitWall(w);
							
			}
			m.draw(g);

		}
		
		for(int i=0;i<wall.size();i++) {
			Wall w=wall.get(i);
			
			
			w.draw(g);			
		}
		for(int i = 0; i < explodes.size(); i++) { 
			Explode e = explodes.get(i);
			e.draw(g);
		}
		for(Wall w:wall) {
			myTank.collidesWithWall(w);
						}
		

		
		for(int i=0;i<tanks.size();i++) {
			for(Wall w:wall) {
			
			
			tanks.get(i).collidesWithWall(w);					
			}		
			tanks.get(i).collidesWithTanks(tanks);			
			tanks.get(i).draw(g);			
		}
	
		 myTank.eat(blood);
			blood.draw(g);
		
		
		
		
		
		//enemyTank.draw(g);
//必须得有这个判断，不然会出错
		if (m != null)
			m.draw(g);

	}

	public void update(Graphics g) {
		if (offScreenImage == null) {
			offScreenImage = this.createImage(GAME_WIDTH, GAME_HEIGHT);
		}
//拿到这个图片的画笔
		Graphics gOffScreen = offScreenImage.getGraphics();

		Color c = gOffScreen.getColor();
		gOffScreen.setColor(Color.GREEN);
		gOffScreen.fillRect(0, 0, GAME_WIDTH, GAME_HEIGHT);
		gOffScreen.setColor(c);
		print(gOffScreen);
		g.drawImage(offScreenImage, 0, 0, null);
	}
 
	public void launchFrame() {
		this.setLocation(300, 50);
		this.setSize(GAME_WIDTH, GAME_HEIGHT);
		this.setTitle("TankWar");
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		Wall w=new Wall(200,40,10,100,this);	
		wall.add(w);
		
		Wall w2=new Wall(200,200,10,100,this);	
		wall.add(w2);
		for(int i = 0; i < 10; i++) {
			Tank t=new Tank(50 + 40 * (i + 1), 50, false, this);						
			tanks.add(t);						
			}
	
	     tanks.add(this.myTank);

		this.setResizable(false);
		this.setBackground(Color.GREEN);

		this.addKeyListener(new KeyMonitor());
		setVisible(true);
		new Thread(new PaintThread()).start();
	}

	public static void main(String[] args) {
		TankClient tc = new TankClient();
		tc.launchFrame();
	}

	private class PaintThread implements Runnable {

		public void run() {
			while (true) {
				repaint();
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}

	}

	private class KeyMonitor extends KeyAdapter {
		public void keyReleased(KeyEvent e) {
			myTank.kyeReleased(e);
			int key=e.getKeyCode();
			if(key==KeyEvent.VK_P) {
				
				if(myTank.isLive()==false) {
				tanks.add(myTank);
					myTank.setLife(100);
					myTank.setLive(true);	
					
					
				}
			}
		}

		public void keyPressed(KeyEvent e) {
			myTank.KyePressed(e);
		}

	}
}